$Host.PrivateData.ProgressBackgroundColor='black'
$Host.PrivateData.ProgressForegroundColor='green'
1..25 | ForEach-Object {
    Write-Progress -Activity "Copying files" -Status "$($_*4) %" -Id 1 -PercentComplete $_ -CurrentOperation "Copying file file_name_$_.txt"
    Start-Sleep -Milliseconds 321}
